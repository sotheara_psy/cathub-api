import axios from 'axios';
import cheerio from 'cheerio';
import debug from 'debug';
import { filterAndInsert } from '../util/scrape-filter';

const log = debug('cathub:funnycatpix');

const totalPageCount = 463;

const baseURL = 'http://www.funnycatpix.com/';

async function loadHtml(url) {
  const response = await axios(url);
  return cheerio.load(response.data);
}

async function scrape(url) {
  const element = url === baseURL ? 'span' : 'div';
  const $ = await loadHtml(url);
  const cats = $('ul li a', '#indexthumbs').map(function cat() {
    const imgSrc = $(this)
      .children('img')
      .attr('src');
    if (!imgSrc) return undefined;
    return {
      title: $(this)
        .children(element)
        .text(),
      imageUrl: `${imgSrc.slice(0, imgSrc.indexOf('_t'))}.jpg`,
    };
  });
  return cats.toArray();
}

export function scrapeCats() {
  for (let i = 1; i <= totalPageCount; i += 1) {
    scrape(i === 1 ? baseURL : `${baseURL}/pictures_${i}.htm`).then(
      async (cats) => {
        await filterAndInsert(cats);
      },
      (err) => {
        console.error(err);
      },
    );
  }
}
