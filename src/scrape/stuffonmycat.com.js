import puppeteer from 'puppeteer';
import { filterAndInsert } from '../util/scrape-filter';

export default async function puppeteerScrapper() {
  const url = 'http://stuffonmycat.com/';
  const browser = await puppeteer.launch({ headless: false });
  const page = await browser.newPage();

  await page.goto(url);
  await page.waitFor(0);

  const totalPages = await page.$eval(
    '#wp_page_numbers > ul > li.first_last_page > a',
    el => el.innerHTML,
  );

  for (let i = 1; i <= totalPages; i += 1) {
    const nexUrl = `${url}page/${i}`;
    await page.goto(nexUrl, { timeout: 0 });

    const cats = await page.evaluate(() => {
      const data = [];
      const bigTitle = document.querySelector('#artcon > h1').innerText;
      const bigImageUrl = document
        .querySelector('#artmain > img')
        .getAttribute('src');

      const elements = document.querySelectorAll('#thumbbox > div.arthumb');
      elements.forEach((el) => {
        let title = el.childNodes[1].innerText;
        let imageUrl = el.childNodes[1].childNodes[0].getAttribute('src');
        imageUrl = imageUrl.replace('-184x184', '');
        title = title.replace(/\n/g, '');
        data.push({ title, imageUrl });
      });
      data.push({ title: bigTitle, imageUrl: bigImageUrl });
      return data;
    });

    await filterAndInsert(cats);
  }
  browser.close();
  console.log('done!');
}
