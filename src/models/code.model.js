import mongoose, { Schema } from 'mongoose';

const codeSchema = new Schema({
  email: String,
  code: String,
  expirationDate: Date,
});

export default mongoose.model('Code', codeSchema);
