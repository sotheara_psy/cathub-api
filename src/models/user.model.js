import mongoose, { Schema } from 'mongoose';

const userSchema = new Schema({
  email: { type: String, unique: true, required: true },
  username: String,
  firstname: String,
  lastname: String,
  gender: String, // null, 'male' or 'female'
  dob: Date,
});
export default mongoose.model('User', userSchema);
