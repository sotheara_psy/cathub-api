import mongoose, { Schema } from 'mongoose';

const voteSchema = new Schema({
  vote: Number,
  catId: {
    type: Schema.Types.ObjectId,
    ref: 'Cat',
  },
  userId: {
    type: Schema.Types.ObjectId,
    ref: 'User',
  },
});

export default mongoose.model('Vote', voteSchema);
