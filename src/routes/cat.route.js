import { createRouter, HttpMethods } from 'express-router-helper';
import {
  postCat,
  getCats,
  removeCat,
  makeVote,
  scrapeDataPuppeteer,
} from '../api/cats.api';
import { scrapeGifCat } from '../scrape/catgifpage';
import { checkAuth } from '../middleware/auth.middleware';

export default createRouter({
  prefix: 'cats',
  routes: [
    {
      method: HttpMethods.POST,
      path: '',
      middleware: [checkAuth()],
      validation: {
        body: {
          rules: {
            title: 'required|string',
            imageUrl: 'required|string',
          },
          messages: {
            required: 'The {{field}} is required.',
            string: 'The {{field}} must be string.',
          },
        },
      },
      handler: postCat,
    },
    {
      method: HttpMethods.DELETE,
      path: ':id',
      middleware: [checkAuth()],
      handler: removeCat,
    },
    {
      method: HttpMethods.GET,
      path: '',
      middleware: [checkAuth('weak')],
      handler: getCats,
    },
    {
      method: HttpMethods.POST,
      path: ':id/upvote',
      middleware: [checkAuth()],
      handler: makeVote(1),
    },
    {
      method: HttpMethods.POST,
      path: ':id/downvote',
      middleware: [checkAuth()],
      handler: makeVote(0),
    },
    {
      method: HttpMethods.POST,
      path: '/scrape',
      middleware: [],
      handler: scrapeGifCat,
    },
    {
      method: HttpMethods.POST,
      path: '/puppeteer',
      middleware: [],
      handler: scrapeDataPuppeteer,
    },
  ],
});
