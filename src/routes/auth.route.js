import { createRouter, HttpMethods } from 'express-router-helper';
import { requestLogin, login } from '../api/auth.api';

export default createRouter({
  prefix: 'auth',
  routes: [
    {
      method: HttpMethods.POST,
      path: 'login',
      middleware: [],
      validation: {
        body: {
          sanitizes: {
            email: 'normalize_email',
          },
          rules: {
            email: 'required|email',
            code: 'required',
          },
          messages: {
            required: 'The {{field}} field is required.',
            email: 'Invalid email address.',
          },
        },
      },
      handler: login,
    },
    {
      method: HttpMethods.POST,
      path: 'request-login',
      middleware: [],
      validation: {
        body: {
          sanitizes: {
            email: 'normalize_email',
          },
          rules: {
            email: 'required|email',
          },
          messages: {
            required: 'The {{field}} field is required.',
            email: 'Invalid email address.',
          },
        },
      },
      handler: requestLogin,
    },
  ],
});
