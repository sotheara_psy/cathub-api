import { createRouter, HttpMethods } from 'express-router-helper';
import { detectAndUpload } from '../api/uploads.api';
import { upload, validateUpload } from '../middleware/uploads.middleware';
import { checkAuth } from '../middleware/auth.middleware';

export default createRouter({
  prefix: 'uploads',
  routes: [
    {
      method: HttpMethods.POST,
      path: 'image',
      middleware: [checkAuth(), upload, validateUpload],
      handler: detectAndUpload,
    },
  ],
});
