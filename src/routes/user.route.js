import { createRouter, HttpMethods } from 'express-router-helper';
import { getUserProfile, updateUser } from '../api/users.api';
import { checkAuth } from '../middleware/auth.middleware';
import {
  noSpaces,
  uniqueUsername,
  allLowercase,
} from '../util/validation.rules';

export default createRouter({
  prefix: 'me',
  routes: [
    {
      method: HttpMethods.GET,
      path: '',
      middleware: [checkAuth()],
      handler: getUserProfile,
    },
    {
      method: HttpMethods.PUT,
      path: '',
      middleware: [checkAuth()],
      validation: {
        body: {
          rules: {
            gender: 'string|in:male,female',
            username: 'string|no_spaces|min:4|unique_username|all_lowercase',
          },
          messages: {
            all_lowercase: 'The {{field}} must be in lowercase',
            unique_username: 'The {{field}} must be unique',
            string: 'The {{field}} must be a string',
            min: 'The {{field}} must be at least 4 charaters',
            no_spaces: 'Bro, you cannot have spaces in your {{field}}.',
          },
        },
        options: {
          extends: [
            /** define global async or custom validations */
            { name: allLowercase.name, fn: allLowercase },
            { name: uniqueUsername.name, fn: uniqueUsername },
            { name: noSpaces.name, fn: noSpaces },
          ],
        },
      },
      handler: updateUser,
    },
  ],
});
