export function toNumber(value, options) {
  const defaults = { min: 0, max: 20, ...options };
  const v = +value;

  if (Number.isNaN(v) || v < 0) {
    if (!defaults.max) return 0;
    return defaults.max;
  } else if (v > defaults.max) {
    if (!defaults.max) return v;
    return defaults.max;
  } else if (v < defaults.min) {
    return defaults.min;
  }
  return v;
}
