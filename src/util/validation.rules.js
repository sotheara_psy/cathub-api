import debug from 'debug';
import User from '../models/user.model';

const log = debug('cathub:validation-rules');

export function uniqueUsername(data, field, message, args, get) {
  return new Promise(async (resolve, reject) => {
    // log('data:', data);
    const usernameValue = get(data, field);
    // log('usernameValue:', usernameValue);
    const user = await User.findOne({ username: usernameValue });
    // log('user:', user);
    if (user) return reject(message);
    return resolve();
  });
}

export function noSpaces(data, field, message, args, get) {
  return new Promise((resolve, reject) => {
    // log('data being validated', data);
    // log('field', field);
    /** @type {string} */
    const usernameValue = get(data, field);
    if (!usernameValue) return resolve('validation skipped'); // let required rule checks this.
    // log('username value:', usernameValue);
    const hasNoSpaces = /^[a-z0-9_]+$/i.test(usernameValue);
    if (hasNoSpaces) return resolve('validation skipped');
    return reject(message); // eslint-disable-line
  });
}
export function allLowercase(data, field, message, args, get) {
  return new Promise((resolve, reject) => {
    const usernameValue = get(data, field);
    log('usernameValue:', usernameValue);
    if (!usernameValue) return resolve('validation skipped');
    const lowercaseUsername = usernameValue.toLowerCase();
    if (usernameValue !== lowercaseUsername) {
      return reject(message);
    }
    resolve();
    // log('data being validated', data);
    // log('field', field);
    // /** @type {string} */
    // const usernameValue = get(data, field);
    // if (!usernameValue) return resolve('validation skipped'); // let required rule checks this.
    // log('username value:', usernameValue);
    // const hasNoSpaces = /^[a-z0-9_]+$/i.test(usernameValue);
    // if (hasNoSpaces) return resolve('validation skipped');
    // return reject(message); // eslint-disable-line
  });
}
