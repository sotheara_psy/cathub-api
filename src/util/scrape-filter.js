import debug from 'debug';
import faker from 'faker';
import Cat from '../models/cat.model';

const log = debug('cathub:srapping-cat:');

export async function filterAndInsert(data) {
  let cats = data;

  // cats = cats.filter(cat => cat.title);

  const imageUrls = cats.map(cat => cat.imageUrl);
  const existCat = await Cat.find({ imageUrl: { $in: imageUrls } });
  const existImgUrl = existCat.map(cat => cat.imageUrl);

  cats = cats.filter(cat => existImgUrl.includes(cat.imageUrl) === false && cat.title);
  cats = cats.map(cat => ({
    title: cat.title,
    imageUrl: cat.imageUrl,
    username: faker.internet.userName().toLowerCase(),
  }));

  log('adding...');
  await Cat.insertMany(cats);
}
