import Cat from '../models/cat.model';

const upvote = 1;
const downvote = 0;

export async function addVoteToCats(vote, catId) {
  const { upvoteCount, downvoteCount } = await Cat.findById(catId);
  if (vote) {
    await Cat.findByIdAndUpdate(catId, { upvoteCount: upvoteCount + 1 });
  } else {
    await Cat.findByIdAndUpdate(catId, { downvoteCount: downvoteCount + 1 });
  }
}

export async function removeVoteFromCats(vote, catId) {
  const { upvoteCount, downvoteCount } = await Cat.findById(catId);
  if (vote) {
    await Cat.findByIdAndUpdate(catId, { upvoteCount: upvoteCount - 1 });
  } else {
    await Cat.findByIdAndUpdate(catId, { downvoteCount: downvoteCount - 1 });
  }
}

export async function updateVoteOfCats(vote, catId) {
  if (vote) {
    addVoteToCats(upvote, catId);
    removeVoteFromCats(downvote, catId);
  } else {
    addVoteToCats(downvote, catId);
    removeVoteFromCats(upvote, catId);
  }
}
