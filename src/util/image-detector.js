import AWS from 'aws-sdk';

const {
  AWS_ACCESS_KEY_ID,
  AWS_SECRET_ACCESS_KEY,
} = process.env;

AWS.config.accessKeyId = AWS_ACCESS_KEY_ID;
AWS.config.secretAccessKey = AWS_SECRET_ACCESS_KEY;

const rekognition = new AWS.Rekognition({ region: 'eu-west-1' });

export default function detectLabelsAsync(buffer) {
  return new Promise((resolve, reject) => {
    rekognition.detectLabels({ Image: { Bytes: buffer } }, (err, data) => {
      if (err) return reject(err);
      return resolve(data);
    });
  });
}
