import jwt from 'jsonwebtoken';

const { JWT_SECRET } = process.env;

export function sign(payload) {
  return new Promise((resolve, reject) => {
    jwt.sign(
      payload,
      JWT_SECRET,
      { issuer: 'CatHub Team', expiresIn: '1 day' },
      (err, result) => {
        if (err) return reject(err);
        return resolve(result);
      },
    );
  });
}

export function verify(token) {
  return new Promise((resolve, reject) => {
    jwt.verify(token, JWT_SECRET, (err, result) => {
      if (err) return reject(err);
      return resolve(result);
    });
  });
}
