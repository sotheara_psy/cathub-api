import multer from 'multer';

const supportedFileTypes = ['image/png', 'image/jpeg', 'image/jpg'];

export const upload = multer({
  storage: multer.memoryStorage(),
  fileFilter: (req, file, cb) => {
    const error = new Error();
    error.name = 'UnsupportedFileTypeError';
    error.message = 'Unsupported file type';
    error.httpCode = 400;
    error.type = 'UNSUPPORTED_FILE_TYPE';
    return supportedFileTypes.some(f => file.mimetype === f)
      ? cb(null, true)
      : cb(error);
  },
  limits: {
    fieldNameSize: 255,
    fileSize: 1024 * 1024 * 5,
  },
}).single('image');

export function validateUpload(req, res, next) {
  if (req.file) {
    next();
  } else {
    res.status(400).json({ message: 'The image field is empty' });
  }
}
