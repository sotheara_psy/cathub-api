import Cat from '../models/cat.model';

export const getUserProfile = async ({ user }, { ok }) => ok(user.toJSON());

export async function updateUser({ body, user }, { ok }) {
  const oldUsername = user.username;
  const {
    gender, username, firstname, lastname, dob,
  } = body;

  const authUser = user;
  authUser.username = username || user.username;
  authUser.firstname = firstname || user.firstname;
  authUser.lastname = lastname || user.lastname;
  authUser.dob = dob || user.dob;
  authUser.gender = gender || user.gender;

  await Promise.all([
    authUser.save(),
    Cat.updateMany({ username: oldUsername }, { $set: { username } }),
  ]);

  ok(authUser.toJSON());
}
