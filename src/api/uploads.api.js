import { extension } from 'mime-types';
import detectLabelsAsync from '../util/image-detector';
import uploadAsync from '../util/cloudinary';

export async function detectAndUpload(req, res) {
  const imageBuffer = req.file.buffer;
  const ext = extension(req.file.mimetype);
  const { Labels: data } = await detectLabelsAsync(imageBuffer);
  const hasCat = data.find(cat => cat.Name.toLowerCase() === 'cat');
  if (!hasCat) {
    res.status(400).json({ message: 'There is no cat in the image.' });
    return;
  }
  const result = await uploadAsync(imageBuffer, {
    format: ext,
  });
  res.json(result);
}
