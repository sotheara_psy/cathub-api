import debug from 'debug';
import moment from 'moment';

import Code from '../models/code.model';
import { sendRequestLoginCode } from '../util/mailer';
import User from '../models/user.model';
import { generateUsername } from '../util/username-generator';
import { sign } from '../util/jwt';

const log = debug('cathub:auth-api');

export async function login({ body: { email, code } }, { bad, ok }) {
  const codesEmail = await Code.findOne({ email });
  if (!codesEmail) {
    bad("Email doesn't exist");
    return;
  }

  const codeObj = await Code.findOne({
    email,
    code,
    expirationDate: { $gt: moment().toISOString() },
  });
  if (!codeObj) {
    bad('Invalid code.');
  }

  const foundUser = await User.findOne({ email });
  if (foundUser) {
    const accessToken = await sign(foundUser.toJSON());
    ok({ accessToken });
  } else {
    const username = await generateUsername(email);
    const newUser = await User.create({ email, username });
    const accessToken = await sign(newUser.toJSON());
    ok({ accessToken });
  }
  await Code.update({ code }, { $set: { code: null } });
}

export async function requestLogin({ body: { email } }, { ok }) {
  const code = await sendRequestLoginCode(email);
  const now = moment();
  const fiveMinLater = now.add('5', 'minutes');
  await Code.create({
    email,
    code,
    expirationDate: fiveMinLater,
  });
  ok();
}
