import debug from 'debug';
import Cat from '../models/cat.model';
import Vote from '../models/vote.model';
import { toNumber } from '../util/query-converter';
import {
  addVoteToCats,
  removeVoteFromCats,
  updateVoteOfCats,
} from '../util/voter';
import puppeteerScrapper from '../scrape/stuffonmycat.com';

const log = debug('cathub:cats-api');

export async function postCat(
  { body: { title, imageUrl }, user: { username } },
  { ok },
) {
  await Cat.create({
    title,
    imageUrl,
    username,
  });
  ok();
}

export async function getCats({ query, user }, { ok }) {
  const { order } = query;
  const offset = toNumber(query.offset, { min: 0, max: null });
  const limit = toNumber(query.limit);

  const [cats, total] = await Promise.all([
    Cat.find({}, {}, { skip: offset, limit }).sort(order),
    Cat.count(),
  ]);

  const catsAsJSON = cats.map((c) => {
    c.vote = null;
    return c.toJSON();
  });
  if (user) {
    const catIds = catsAsJSON.map(c => c._id);
    log(catIds);

    const votes = await Vote.find({ userId: user.id, catId: { $in: catIds } });
    catsAsJSON.forEach((cat) => {
      votes.forEach((vote) => {
        cat.vote =
          cat._id.toString() === vote.catId.toString() ? +vote.vote : null;
      });
    });
  }

  const metadata = {
    limit,
    total,
    offset,
  };
  ok({ data: catsAsJSON, metadata });
}

export async function removeCat({ params, user }, { ok, bad }) {
  const catId = params.id;
  const userEmail = user.email;

  const cat = await Cat.find({ _id: catId, owner: userEmail });
  if (!cat) {
    bad('The cat was not found.');
  }

  await Cat.remove({ _id: catId });
  ok();
}
/**
 * Handler for upvote & downvote.
 *
 * @param {number} v Should be 0 or 1.
 */
export function makeVote(v) {
  return async ({ params: { id: catId }, user: { _id: userId } }, { ok }) => {
    const vote = await Vote.findOne({ userId, catId });
    if (vote) {
      if (vote.vote === v) {
        await removeVoteFromCats(v, catId); // updateVoteCount(catId, inc or dec)
        await Vote.remove({ userId, catId });
        log('vote removed');
      } else {
        await updateVoteOfCats(v, catId);
        await Vote.updateOne({ userId, catId }, { $set: { vote: v } });
        log('vote updated');
      }
    } else {
      await addVoteToCats(v, catId);
      await Vote.create({ vote: v, catId, userId });
      log('vote added');
    }
    ok();
  };
}

export async function scrapeDataPuppeteer(req, { ok }) {
  puppeteerScrapper('test');
  // ok();
}
