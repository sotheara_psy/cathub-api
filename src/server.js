import 'dotenv/config';

import debug from 'debug';
import express from 'express';
import mongoose from 'mongoose';
import cors from 'cors';

import defaultRoute from './routes/default.route';
import authRoute from './routes/auth.route';
import uploadRoute from './routes/upload.route';
import catRoute from './routes/cat.route';
import userRoute from './routes/user.route';
import { responses } from './util/responses';

const { NODE_ENV, PORT, DB_URI } = process.env;
const log = debug('cathub:server');
const app = express();

// application middleware
app.use(responses());
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(express.static('public'));
app.use(cors());

// routers
app.use(defaultRoute, authRoute, uploadRoute, catRoute, userRoute);

// error handler
app.use((err, req, res, next) => {
  const errResponse = err.response || {};
  res.status(err.status || 500);
  res.json({
    message: err.message,
    stack: process.env.NODE_ENV !== 'production' ? err.stack : undefined,
    ...errResponse,
  });
  next();
});

(async () => {
  try {
    await mongoose.connect(DB_URI);
    log('MongoDB Connected!');
    app.listen(PORT, () => {
      log(`Server environment ${NODE_ENV}`);
      log(`Server listening on port ${PORT}`);
    });
  } catch (e) {
    console.error(e);
    process.exit(1);
  }
})();
