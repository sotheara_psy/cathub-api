# CatHub API

CatHub REST API is an Open Source Project aim to teach how to build REST API with Node.js.

[![forthebadge](https://forthebadge.com/images/badges/contains-cat-gifs.svg)](https://forthebadge.com)
[![forthebadge](https://forthebadge.com/images/badges/made-with-javascript.svg)](https://forthebadge.com)
[![forthebadge](https://forthebadge.com/images/badges/built-with-love.svg)](https://forthebadge.com)
[![forthebadge](https://forthebadge.com/images/badges/built-by-developers.svg)](https://forthebadge.com)

## Usage

* Install dependencies

```sh
npm install
```

* Run in Watch Mode (Development)

```sh
npm run dev
```

* Run normally (Production)

```sh
npm start
```

## Environment variables

Create `.env` file to apply environment variables.

## Misc

![CatHub](https://media.giphy.com/media/vFKqnCdLPNOKc/giphy.gif)

## LICENSE

MIT
